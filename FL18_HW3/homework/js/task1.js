const souzand = 1000
const hundred = 100
const one = 1 
const two = 2
let initialMoney
let numberOfYears
let percentage

const calculateProfit = () => {
    let amount = initialMoney

    for (let i = 0; i < numberOfYears; i++) {
        amount += amount / hundred * percentage
    }

    alert(`
    Initial amount: ${initialMoney}
    Number of years: ${numberOfYears}
    Percentage of year: ${percentage}\n
    Total profit: ${(amount - initialMoney).toFixed(two)}
    Total amount: ${amount.toFixed(two)}
    `)
}

const validInputs = () => {
    initialMoney = parseInt(prompt('Inputs initial amount of money'))
    while (initialMoney < souzand || isNaN(initialMoney)) {
        alert('Invalid input data') 
        initialMoney = parseInt(prompt('Inputs initial amount of money'))
    } numberOfYears = parseInt(prompt('Inputs number of years'))

    while (numberOfYears < one || isNaN(numberOfYears)) {
        alert('Invalid input data') 
        numberOfYears = parseInt(prompt('Inputs number of years'))
    } percentage = parseInt(prompt('Inputs percentage of a year')) 

    while (percentage >= hundred || isNaN(percentage)) {
        alert('Invalid input data') 
        percentage = parseInt(prompt('Inputs percentage of a year'))
    } calculateProfit()

    validInputs()
}

validInputs()