const appRoot = document.getElementById('app-root');

appRoot.insertAdjacentHTML(
    'afterbegin',
    `
    <header>
    <form class="application">
        <h1 class="application_title">Countries Search</h1>
        <div class="application_type-block">
            <p>Please choose type of search:</p>
            <div class ="radio-block">
            <input name="radio" id="radio-region" type="radio" value="region">
            <label for="radio-region">By Region</label>
            <br>
            <input name="radio" id="radio-language" type="radio" value="language">
            <label fot="radio-language">By Language</label>
            </div>
        </div>
        <div class="application_search-block">
            <label for="query">Please choose search query: </label>
            <select name="query" id="query" class="width" disabled>
                <option value>Select value</option>
            </select>
        </div>
        <p class="none">No items, please choose search query</p>
      </form>
      <main id="resultOfChoice">
    </header>
  `
)

const querySelect = document.querySelector('#query')
const radioButtonRegion = document.querySelector('#radio-region')
const radioButtonLanguage = document.querySelector('#radio-language')
const resultOfChoice = document.querySelector('#resultOfChoice')
const noItems = document.querySelector('.none')
const arrOfLang = externalService.getLanguagesList()
const arrOfRegion = externalService.getRegionsList()
let renderColum
let radioButtonSelect

radioButtonRegion.addEventListener(
    'change', event => {
        querySelect.disabled = !event.target.value
        fillSelectOptions(arrOfRegion)
        radioButtonSelect = radioButtonRegion.value
    }, false
)

radioButtonLanguage.addEventListener(
    'change', event => {
        querySelect.disabled = !event.target.value
        fillSelectOptions(arrOfLang)
        radioButtonSelect = radioButtonLanguage.value
    }, false
)

function fillSelectOptions(arr) {
    querySelect.disabled === true ? querySelect.disabled = false : true
    noItems.style.display === '' ? noItems.style.display = 'block' : true

    querySelect.innerHTML = ''
    querySelect.insertAdjacentHTML(
        'beforeend',
        `
        <option value>Select value</option>
      ${arr.map(e => `<option value="${e}">${e}</option>`).join('')}
        `
    )
    querySelect.children[0].selected = 'true'
}

querySelect.addEventListener('change', function () {
    noItems.style.display === 'block' ? noItems.style.display = 'none' : true
    let arrsearch;
    if (appRoot.querySelector('.table') !== null) {
        appRoot.querySelector('.table').remove()
    }
    if (radioButtonSelect === 'region') {
        arrsearch = externalService.getCountryListByRegion(querySelect.value)
    } else if (radioButtonSelect === 'language') {
        arrsearch = externalService.getCountryListByLanguage(querySelect.value)
    }
    let newArr = []
    for (let elem of arrsearch) {
        let rowOfTable = []
        rowOfTable.push(elem.name)
        rowOfTable.push(elem.capital)
        rowOfTable.push(elem.region)

        let listLanguage = []
        for (let key in elem.languages) {
            if (elem.languages.hasOwnProperty(key)) {
                listLanguage.push(' ' + elem.languages[key])
            }
        }
        rowOfTable.push(listLanguage)
        rowOfTable.push(elem.area)
        let flagEl = `<img src = ${elem.flagURL} alt = "img of flag">`
        rowOfTable.push(flagEl)
        newArr.push(rowOfTable)
    }
    renderColum = newArr[0].length
    newArr.sort()

    createTable(resultOfChoice, renderColum, newArr.length, newArr)
});

function createTable(parent, cols, rows, arr) {
    appRoot.insertAdjacentHTML(
        'beforeend',
        `
        <table class="table" id="table">
            <thead class="table__head">
                <tr>
                    <th>Country name
                        <button id="buttonName" class="btn-arrow">&#8593;</button>
                    </th>
                    <th>Capital</th>
                    <th>World Region</th>
                    <th>Languages</th>
                    <th>Area
                        <button id="buttonArea" class="btn-arrow">&#8597;</button>
                    </th>
                    <th>Flag</th>
                </tr>
            </thead>
            <tbody class="table__body">
            </tbody>
        </table>
      `
    )
    for (let i = 0; i < rows; i++) {
        let tr = document.createElement('tr');
        for (let j = 0; j < cols; j++) {
            let td = document.createElement('td');
            td.innerHTML = `${arr[i][j]}`
            tr.append(td)
        } table.append(tr)
    }
} 