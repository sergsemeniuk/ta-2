function visitLink(path) {
	let curVal = localStorage.getItem(path)
	let newVal = ++curVal
	localStorage.setItem(path, newVal)
}

function viewResults() {
	const page1 = localStorage.getItem('Page1') || 0,
		page2 = localStorage.getItem('Page2') || 0,
		page3 = localStorage.getItem('Page3') || 0

	if (document.querySelector('#visitedList') === null) {
		const list = document.querySelector('#content').appendChild(document.createElement('ul'))
		list.id = 'visitedList'

		list.appendChild(document.createElement('li')).textContent = `You visited Page 1 ${page1} time(s)`
		list.appendChild(document.createElement('li')).textContent = `You visited Page 2 ${page2} time(s)`
		list.appendChild(document.createElement('li')).textContent = `You visited Page 3 ${page3} time(s)`
	} localStorage.clear()
}
