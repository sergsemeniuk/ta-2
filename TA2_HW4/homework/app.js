function reverseNumber(num) {
    let reversNum = ''
    num = `${num}`
    for (let i = num.length - 1; num[i] > 0; i--) {
        reversNum += num[i]
    } return num > 0 ? +`${reversNum}` : `${-reversNum}`
}  

function forEach(arr, func) {
    for (const el of arr) {
        func(el)
    }
}

function map(arr, func) {
    const newArr = []
    forEach(arr, el => newArr.push(func(el)))
    return newArr
}

function filter(arr, func) {
    const arrFilter = []
    forEach(arr, el => func(el) ? arrFilter.push(el) : false)
    return arrFilter
}

function getAdultAppleLovers(data) {
    const allowedAge = 18 
    const favFruit = 'apple'
    return map(filter(data, el => el.age > allowedAge && el.favoriteFruit === favFruit), el => el.name)
}

function getKeys(obj) {
    const keyArr = []
    for (const el in obj) {
        if (obj.hasOwnProperty(el)) {
            keyArr.push(el)
        }
    } return keyArr
}

function getValues(obj) {
    const valuesArr = []
    for (const el in obj) {
        if (obj.hasOwnProperty(el)) {
            valuesArr.push(obj[el])
        }
    } return valuesArr
}

function showFormattedDate(dateObj) {
    const lessThan = 10
    const day = dateObj.getDate()
    const month = dateObj.toLocaleString('default', {month: 'short'})
    const year = dateObj.getFullYear()
    return `It is ${day < lessThan ? '0' + day: day} of ${month}, ${year}`
}

