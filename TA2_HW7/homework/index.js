const startPoint = 1970,
    yy = 1000, 
    mm = 3600, 
    hh = 24

const getAge = birth => 
    Math.floor(new Date(Date.now() - birth.getTime()).getUTCFullYear() - startPoint)

const getWeekDay = today => 
    new Intl.DateTimeFormat('en-US', { weekday: 'long'}).format(today)

const getAmountDaysToNewYear = () => 
    Math.floor((new Date('January 1, 2022') - new Date()) / (yy*mm*hh))

const getProgrammersDay = year => {
    const numCelebratedDay = 256
    const startYear = new Date(year, 0, 0)
    const thisYearProgDay = new Date(startYear.setDate(startYear.getDate() + numCelebratedDay))
    const month = thisYearProgDay.toLocaleString('en-US', { month: 'short' })
    return `${thisYearProgDay.getDate()} ${month}, ${thisYearProgDay.getFullYear()} (${getWeekDay(thisYearProgDay)})`
}

const howFarIs = day => {
    const currentDay = day.charAt(0).toUpperCase() + day.slice(1)
    const days = ['Sunday', 'Monday','Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    const indexOfCurrentDay = days.indexOf(currentDay)
    const todayNum = new Date().getDay()
    const dayNum = indexOfCurrentDay - (todayNum < indexOfCurrentDay ? todayNum : todayNum - days.length)

    if (currentDay === getWeekDay()) {
        return `Hey,today is ${currentDay} =)`
    } return `It's ${dayNum} day(s) left till ${currentDay}`
} 

const isValidIdentifier = str => 
    /^[a-z_$][a-z0-9_$]*$/i.test(str)

const capitalize = str => 
    str.replace(/^(.)|\s+(.)/g, c => c.toUpperCase())

const isValidAudioFile = str => 
    /^([a-zA-Z][^_]+)\.(mp3|flac|aac)$/.test(str)

const getHexadecimalColors = str => 
    str.match(/#(?:[0-9a-fA-F]{3}){1,2}\b/ig) || []

const isValidPassword = str => 
    /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/gm.test(str)

const addThousandsSeparators = num => 
    num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')

const getAllUrlsFromText = str => 
    !str ? 'error' : str.match(/(https?:\/\/[^ ]*)/g) || []