const isEquals = (a, b) => a === b 

const isBigger = (a, b) => a > b 

const storeNames = (...str) => str

const getDifference = (a, b) => a > b ? a - b : b - a 

const negativeCount = arr => arr.filter(num => num < 0).length

const letterCount = (word, letter) => word.split('').filter(char => char === letter).length

const countPoints = games => {
   return games.reduce((sum, el) => {
      const bigger = 3
      el = el.split(':')
      if (Number(el[0]) > Number(el[1])) {
         sum += bigger
      } else if (Number(el[0]) === Number(el[1])) {
         sum += 1
      } return sum
   }, 0)
}



