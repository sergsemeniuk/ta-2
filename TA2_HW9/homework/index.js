/* START TASK 1: Your code goes here */
function initialization() {
    const specialCell = document.querySelector('#special-cell');

    checkCells();

    specialCell.removeEventListener('click', yellowCell);
    specialCell.addEventListener('click', greenCell);
} initialization();

function checkCells() {
    const rowArr = document.querySelectorAll('.table-row');

    for (let i = 0; i < rowArr.length; i++) {
        const els = rowArr[i].getElementsByClassName('table-cell');
        els[0].addEventListener('click', blueCell);
        for (let j = 1; j < els.length; j++) {
            els[j].addEventListener('click', yellowCell);
        }
    }
}

function yellowCell(e) {
    e.target.classList.remove('has-white-bg');
    e.target.classList.remove('has-blue-bg');
    e.target.classList.remove('has-green-bg');
    e.target.classList.toggle('has-yellow-bg');
}

function blueCell(e) {
    const tr = e.target.parentNode;
    const cellArrCurrent = tr.querySelectorAll('.table-cell');

    if (e.target === cellArrCurrent[0]) {
        for (let i = 0; i < cellArrCurrent.length; i++) {
            if (!cellArrCurrent[i].classList.contains('has-yellow-bg')) {
                cellArrCurrent[i].classList.remove('has-white-bg');
                cellArrCurrent[i].classList.remove('has-green-bg');
                cellArrCurrent[i].classList.toggle('has-blue-bg');
            }
        }
    }

}

function greenCell() {
    const els = [...document.getElementsByClassName('has-white-bg')];
    const table = document.querySelector('.table');
    table.classList.toggle('has-green-bg');

    for (let i = 0; i < els.length; i++) {
        els[i].classList.toggle('has-green-bg');
        els[i].classList.remove('has-white-bg');
    }
}
/* END TASK 1 */

/* START TASK 2: Your code goes here */
const validBtn = document.getElementById('validBtn');
const phoneInput = document.getElementById('phoneInput');
const phoneNumberForm = document.getElementById('phoneNumberForm');
const validArea = document.createElement('div');
validArea.classList = 'valid-area';

function init() {
    phoneInput.addEventListener('change', checkResultFalse);
    validBtn.addEventListener('click', checkResultTrue);
} init();

function checkResultFalse() {
    if (!phoneInput.checkValidity()) {
        validBtn.disabled = true;
        phoneNumberForm.insertBefore(validArea, phoneNumberForm.firstChild);
        phoneNumberForm.style.marginTop= '41px';
        phoneInput.style.border = '1px solid red';
        validArea.style.backgroundColor = '#FF8080';
        validArea.innerHTML = `Type number does not follow format +380*********`;
    } else {
        validBtn.disabled = false;
    }
}

function checkResultTrue() {
  if(phoneInput.checkValidity()) {
    phoneNumberForm.insertBefore(validArea, phoneNumberForm.firstChild);
    phoneNumberForm.style.marginTop = '62px';
    validBtn.disabled = false;
    validArea.style.backgroundColor = 'green';
    phoneInput.style.border = '1px solid black';
    validArea.textContent = 'Data was succesfully sent';
    }
}
/* END TASK 2 */

/* START TASK 3: Your code goes here */
const field = document.getElementById('field');
field.addEventListener('click', game)
const ball = document.getElementById('ball');

function game(event) {
    let fieldCoords = this.getBoundingClientRect();
    const numberTwo = 2;

    let ballCoords = {
        top: event.clientY - fieldCoords.top - field.clientTop - ball.clientHeight / numberTwo,
        legt: event.clientX - fieldCoords.left - field.clientLeft - ball.clientHeight / numberTwo
    };

    if (ballCoords.top < 0) {
        ballCoords.top = 0;

    } else if (ballCoords.legt < 0) {
        ballCoords.legt = 0;

    } else if (ballCoords.legt + ball.clientWidth > field.clientWidth) {
        ballCoords = field.clientWidth - ball.clientWidth;

    } else if (ballCoords.top + ball.clientHeight > field.clientHeight) {
        ballCoords.top = field.clientHeight - ball.clientHeight;
    }

    ball.style.left = ballCoords.legt + 'px';
    ball.style.top = ballCoords.top + 'px';
}

const scoreA = document.getElementById('score-a');
scoreA.textContent = '0';

const scoreB = document.getElementById('score-b');
scoreB.textContent = '0';

const basketA = document.getElementById('basket-a');
basketA.addEventListener('click', goal);

const basketB = document.getElementById('basket-b');
basketB.addEventListener('click', goal);

function goal(e) {
    const milliseconds = 3000;

    increaseScore(e.target === basketA ? 'score-b' : 'score-a');

    renderNotification(e.target === basketA ? 'Team B' : 'Team A');
    
    e.target === basketA ? scoreTxt.style.color = 'blue' : scoreTxt.style.color = 'red';

    setTimeout(stopShowNotification, milliseconds);
}

function increaseScore(id) {
    const score = parseInt(document.getElementById(id).textContent) + 1;

    document.getElementById(id).textContent = score.toString();
}

const scoreTxt = document.getElementById('score-txt');

function renderNotification(teem) {
    scoreTxt.classList.remove('has-display-none');
    scoreTxt.textContent = `${teem} score`;
}

function stopShowNotification() {
    scoreTxt.classList.add('has-display-none');
}
/* END TASK 3 */
